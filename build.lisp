
(in-package :common-lisp-user)

(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :woo)

;; starts up the web server
(defun run ()
  (woo:run
   (lambda (env)
     (declare (ignore env))
     '(200 (:content-type "text/html") ("<!DOCTYPE html><h1>Hello, World</h1><p>This page is served from Google Cloud Run, SBCL and woo!! Exciting eh?</p>")))
   :address "0.0.0.0"
   :port (or (if (sb-ext:posix-getenv "PORT")
                 (parse-integer (sb-ext:posix-getenv "PORT")))
             8080)))


(sb-ext:save-lisp-and-die "/app/app" :executable t
                          :toplevel 'run)

