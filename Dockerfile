FROM ppymdjr/sbcl:latest as builder
WORKDIR /app
ADD ./build.lisp /app/build.lisp
RUN cd /app; \
    sbcl --load build.lisp
    
    
FROM debian:stretch
COPY --from=builder /app/app /app
RUN apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install sbcl shadowsocks-libev


CMD ["/app"]
